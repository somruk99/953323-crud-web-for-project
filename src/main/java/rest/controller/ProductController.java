package rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import rest.service.ProductService;
import rest.entity.*;

@Controller
public class ProductController {

    @Autowired
    ProductService productService;
    @GetMapping("/products")
    public ResponseEntity getAllProducts() {
        return ResponseEntity.ok(this.productService.getProducts());
    }

    @GetMapping("/products/{id}")
    public ResponseEntity getProductById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.productService.getProduct(id));
    }

    @PostMapping("/products")
    public ResponseEntity saveProduct(@RequestBody Product product) {
        return ResponseEntity.ok(this.productService.saveProduct(product));
    }
}
