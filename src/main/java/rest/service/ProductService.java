package rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rest.ProductDao;
import rest.entity.*;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductDao productDao;
    public List<Product> getProducts() {
        return this.productDao.getProducts();
    }
    public Product getProduct(Long id) {
        return this.productDao.getProduct(id);
    }

    public Product saveProduct(Product product) {
        return this.productDao.saveProduct(product);
    }
}
