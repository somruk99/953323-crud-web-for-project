package rest;

import org.springframework.stereotype.Repository;
import rest.entity.*;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductDao {
    List<Product> products;

    public ProductDao() {
        products = new ArrayList<Product>() ;
        this.products.add(
                Product.builder()
                .id(1l)
                .name("Chocolate")
                .price(30d)
                .amount(2)
                .category("Food")
                .picture("https://www.theshopofgoodtaste.com/wp-content/uploads/2020/01/isvaari-60-dark-chocolate-with-assorted-nuts-chenab-impex-600x600.jpg")
                .description("Some sweet snack for you people")
                        .build()
            );

    }
    public List<Product> getProducts() {
        return this.products;
    }

    public Product getProduct(Long id) {
        return this.products.get(Math.toIntExact(id-1));
    }

    public Product saveProduct(Product product) {
        product.setId((long) this.products.size());
        this.products.add(product);
        return product;
    }
}
